#!/usr/bin/python3
"""
Simple TODO list indicator with notification messanger.
Tasks are loaded from json file.
Indicator (ubuntu build-in + gtk) provides a list of tasks.
Each tasks can be toggled as done/undone.
Notifier sends message in prefered timeout delay.
Notifications mechanism can be enabled and disabled.
Edit tasks menu item launches an edit window.
"""
import datetime
import json
import os
import signal
import re

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')
gi.require_version('Notify', '0.7')
from gi.repository import AppIndicator3 as ai
from gi.repository import GObject as go
from gi.repository import Gtk as gtk
from gi.repository import Gdk as gdk
from gi.repository import Notify as no
from gi.repository import GLib as gl

import gobject

TODO_LIST_FILENAME = "list.json"
ICON_FILE = "tododone.svg"
TODO_LIST_PATH = os.path.abspath(
    os.path.dirname(__file__)) + "/" + TODO_LIST_FILENAME
ICON_PATH = os.path.abspath(os.path.dirname(__file__)) + "/" + ICON_FILE
APPINDICATOR_ID = 'TODOeveryDAY'


class Tasker:
    """
    Tasker class implements functions needed to modify 
    task list state and control the noficiation manager.
    """

    def __init__(self):
        self.todays_date = datetime.datetime.now().strftime("%d-%m-%Y")
        self.tasks = self.load_tasks()
        self.notify = no.init(APPINDICATOR_ID)
        self.notify_title = "TODAYS TASKS:"
        self.notify_done_msg = "Everything is done today! GJ!"
        self.notify_null_list_msg = "The TODO list is empty."
        self.notify_enabled = True
        self.notify_time_delay_ms = 1000 * 60 * 5  # 5 minutes
        self.notify_iterator = go.timeout_add(
            self.notify_time_delay_ms, self.tasks_notify)

    def load_tasks(self):
        """Load all todo tasks from json list"""
        with open(TODO_LIST_PATH, "r") as f:
            tasks_dicts_list = json.load(f)
        return tasks_dicts_list

    def update_tasks_file(self):
        with open(TODO_LIST_PATH, "w") as f:
            json.dump(self.tasks, f)

    def setup_task_state(self, task):
        if task['date'] == self.todays_date:
            return 1
        return 0

    def toggle_nofity_enabled_state(self, wigdet, data=None):
        self.notify_enabled = not self.notify_enabled

    def toggled_task_item_state(self, wigdet, task):
        # Update task status
        label = task.get_label()
        present_task = next(
            (item for item in self.tasks if item['name'] == label), None)
        if present_task['date'] == self.todays_date:
            date = "Null"
            if self.notify_iterator == None:
                self.notify_iterator = go.timeout_add(
                    self.notify_time_delay_ms, self.tasks_notify)
        else:
            date = self.todays_date
        for item in self.tasks:
            if item['name'] == label:
                item['date'] = date
        self.update_tasks_file()

    def remove_tasks(self, selected_tasks):
        for task in selected_tasks:
            self.tasks = [d for d in self.tasks if d != task]
        self.update_tasks_file()

    def set_item_timeout_state(self, timeout):
        return True if timeout * 1000 * 60 == self.notify_time_delay_ms else False

    def notify_timeout_cb(self, item, timeout, timeout_items):
        """
        Callback that handles activation of a timeout delay variable.
        Only clicked timeout item will be set as enabled and timeout value
        will be updated.
        """
        if self.notify_time_delay_ms == 60000 * timeout:
            item.props.active = True
        if item.props.active == True:
            if self.notify_iterator != None:
                go.source_remove(self.notify_iterator)
            self.notify_time_delay_ms = 60000 * timeout
            self.notify_iterator = go.timeout_add(
                self.notify_time_delay_ms, self.tasks_notify)
            for timeout_item in timeout_items:
                if timeout_item != item:
                    if timeout_item.props.active == True:
                        timeout_item.props.active = False

    def tasks_notify(self):
        if self.notify_enabled:
            tasks_list = sorted(self.load_tasks(),
                                key=lambda item: item['priority'])
            notify_message = ""
            for task in tasks_list:
                if task['date'] != self.todays_date:
                    notify_message += "{p} - {name} \n".format(
                        p=task['priority'], name=task['name'])
            if notify_message != "":
                notification = no.Notification.new(
                    self.notify_title, notify_message)
                notification.show()
                return True
            else:
                notification = no.Notification.new(
                    self.notify_title, self.notify_done_msg)
                notification.show()
                if self.notify_iterator != None:
                    go.source_remove(self.notify_iterator)
                    self.notify_iterator = None
                return False
        return True


def build_options_menu(menu, tasker, indicatorMain):
    """ Setup indicators configuration options items menu"""
    items_list = []
    # Separation line after task list
    item_separator = gtk.SeparatorMenuItem()
    items_list.append(item_separator)
    # Configuration options
    item_edit_tasks = gtk.MenuItem('Edit tasks')
    item_edit_tasks.connect('activate', edit_tasks,
                            menu, indicatorMain, tasker)
    items_list.append(item_edit_tasks)

    notify_enable_item = gtk.CheckMenuItem.new_with_label("Notify enabled")
    notify_enable_item.set_active(tasker.notify_enabled)
    notify_enable_item.connect('toggled', tasker.toggle_nofity_enabled_state)
    items_list.append(notify_enable_item)

    timeout_menu_item = gtk.MenuItem.new_with_label("Notify timeout")
    timeout_menu = gtk.Menu()
    timeouts = [5, 15, 30, 60]
    labels = ['5 minutes', '15 minutes', '30 minutes', '1 hour']
    assert(len(labels) == len(timeouts))
    timeout_items = []
    # Setup timeouts labels states as toggled and precluded
    for timeout, label in zip(timeouts, labels):
        timeout_items.append(gtk.CheckMenuItem.new_with_label(label))

    for timeout, timeout_item in zip(timeouts, timeout_items):
        timeout_item.set_active(tasker.set_item_timeout_state(timeout))
        timeout_item.connect(
            'activate', tasker.notify_timeout_cb, timeout, timeout_items)
        timeout_menu.append(timeout_item)

    timeout_menu_item.set_submenu(timeout_menu)
    items_list.append(timeout_menu_item)

    item_quit = gtk.MenuItem('Quit')
    item_quit.connect('activate', quit)
    items_list.append(item_quit)

    return items_list


def build_menu(taskerMain, indicatorMain):
    """Setup indicator gtk menu """
    menu = gtk.Menu()

    # Task list as togglable indicator items
    task_list = sorted(taskerMain.tasks, key=lambda k: k['name'])
    if task_list:
        for task in task_list:
            task_item = gtk.CheckMenuItem.new_with_label(task["name"])
            task_item.set_active(taskerMain.setup_task_state(task))
            task_item.connect(
                "toggled", taskerMain.toggled_task_item_state, task_item)
            menu.append(task_item)

    for item in build_options_menu(menu, taskerMain, indicatorMain):
        menu.append(item)

    menu.show_all()
    return menu


class EditWindow(gtk.Window):
    """ 
    EditWindow class implements functions needed to 
    edit view for to-do list tasks and indicator state.
    """

    def __init__(self, menu, indicator, tasker):
        gtk.Window.__init__(self, title="Edit TODO tasks")

        self.set_border_width(10)
        self.menu = menu
        self.indicator = indicator
        self.tasker = tasker

        self.grid = gtk.Grid()
        self.grid.set_column_homogeneous(True)
        self.grid.set_row_homogeneous(True)
        self.add(self.grid)
        self.task_liststore = gtk.ListStore(str, str, str)
        self.task_dict_list = sorted(
            self.tasker.tasks, key=lambda k: k['name'])
        # Prefered keys list(backup for table column titles)
        self.task_keys = sorted(["priority", "name", "date"], reverse=True)
        self.task_list = []
        for d in self.task_dict_list:
            self.task_list.append([d[k] for k in self.task_keys])
        # Setup the order as p , name ,date
        for task in self.task_list:
            self.task_liststore.append(list(task))
            self.current_filter_language = None
        # Creating the ListStore model
        self.language_filter = self.task_liststore.filter_new()
        # Setting the filter function
        self.language_filter.set_visible_func(self.priority_filter_func)
        self.treeview = gtk.TreeView.new_with_model(self.language_filter)
        for i, column_title in enumerate(self.task_keys):
            renderer = gtk.CellRendererText()
            column = gtk.TreeViewColumn(column_title, renderer, text=i)
            self.treeview.append_column(column)
        self.buttons = list()
        self.priority_list = ["P1", "P2", "P3"]
        for priority in self.priority_list + ["All"]:
            button = gtk.Button(priority)
            self.buttons.append(button)
            button.connect("clicked", self.on_selection_button_clicked)
        # Treeview with columns and filters
        self.scrollable_treelist = gtk.ScrolledWindow(
            hexpand=True, vexpand=True)
        self.scrollable_treelist.set_min_content_height(150)
        self.grid.attach(self.scrollable_treelist, 0, 0, 4, 2)

        self.labelshow = gtk.Label(" Show: ")
        self.grid.add(self.labelshow)

        self.grid.add(self.buttons[0])
        for i, button in enumerate(self.buttons[1:]):
            self.grid.attach_next_to(
                button, self.buttons[i], gtk.PositionType.BOTTOM, 1, 1)
        self.scrollable_treelist.add(self.treeview)

        # Button to add new titles
        self.button_add = gtk.Button(label="Add")
        self.button_add.connect("clicked", self.on_button_add_clicked)

        self.entryName = gtk.Entry()
        self.entryDate = gtk.Entry()
        self.entryDate.set_text(self.tasker.todays_date)

        self.priority_selected = None
        self.priority_combo = gtk.ComboBoxText()
        self.priority_combo.connect("changed", self.on_priority_combo_changed)
        for priority in self.priority_list:
            self.priority_combo.append_text(priority)
        self.priority_combo.set_entry_text_column(0)
        self.priority_combo.set_active(0)
        # Button to remove specified titles
        self.button_remove = gtk.Button(label="Remove")
        self.button_remove.connect("clicked", self.on_button_remove_clicked)

        # Button to remove all titles
        self.button_remove_all = gtk.Button(label="Remove All")
        self.button_remove_all.connect(
            "clicked", self.on_button_remove_all_clicked)

        self.labelEdit = gtk.Label()
        self.labelEdit.set_text("Edit:")
        self.labelPriority = gtk.Label()
        self.labelPriority.set_text("Priority:")
        self.labelName = gtk.Label()
        self.labelName.set_text("Name:")
        self.labelDate = gtk.Label()
        self.labelDate.set_text("Date:")
        self.grid.attach(self.labelEdit, 0, 2, 4, 1)
        self.grid.attach(self.button_add, 0, 3, 1, 1)
        self.labelEdit
        self.grid.attach_next_to(
            self.labelPriority, self.button_add, gtk.PositionType.RIGHT, 1, 1)
        self.grid.attach_next_to(
            self.labelName, self.labelPriority, gtk.PositionType.RIGHT, 1, 1)
        self.grid.attach_next_to(
            self.labelDate, self.labelName, gtk.PositionType.RIGHT, 1, 1)
        self.grid.attach_next_to(
            self.priority_combo, self.labelPriority, gtk.PositionType.BOTTOM, 1, 1)
        self.grid.attach_next_to(
            self.entryName, self.labelName, gtk.PositionType.BOTTOM, 1, 1)
        self.grid.attach_next_to(
            self.entryDate, self.labelDate, gtk.PositionType.BOTTOM, 1, 1)
        self.grid.attach_next_to(
            self.button_remove, self.button_add, gtk.PositionType.BOTTOM, 1, 1)
        self.grid.attach_next_to(
            self.button_remove_all, self.button_remove, gtk.PositionType.BOTTOM, 1, 1)
        self.show_all()

    def indi_menu_refresh_cb(self, menu, tasker, indicator):
        items = menu.get_children()
        for item in items:
            menu.remove(item)

        task_list = sorted(tasker.tasks, key=lambda k: k['name'])
        if task_list:
            for task in task_list:
                task_item = gtk.CheckMenuItem.new_with_label(task["name"])
                task_item.set_active(tasker.setup_task_state(task))
                task_item.connect(
                    "toggled", tasker.toggled_task_item_state, task_item)
                menu.append(task_item)

        for item in build_options_menu(menu, tasker, indicator):
            menu.append(item)

        menu.show_all()
        indicator.set_menu(menu)

    def priority_filter_func(self, model, iter, data):
        """Tests if the language in the row is the one in the filter"""
        if self.current_filter_language is None or self.current_filter_language == "All":
            return True
        else:
            return model[iter][0] == self.current_filter_language

    def update_treeview(self):
        self.task_liststore.clear()
        if self.tasker.tasks:
            for task in sorted(self.tasker.tasks,
                               key=lambda item: item['priority']):
                self.task_liststore.append(
                    list([task[k] for k in self.task_keys]))
        self.treeview.set_model(self.language_filter)

    def on_selection_button_clicked(self, widget):
        """Called on any of the button clicks"""
        # Set the current language filter to the button's label
        self.current_filter_language = widget.get_label()
        # Update the filter, which updates in turn the view
        self.language_filter.refilter()

    def on_button_remove_clicked(self, widget):

        # The first element is a ListStore
        # The second element is a list of tree paths
        # of all selected rows
        (model, paths) = self.treeview.get_selection().get_selected_rows()

        task = {}
        # Get the TreeIter instance for each path
        for path in paths:
            iter = model.get_iter(path)
        # Remove the ListStore row referenced by iter
            if iter is not None:
                for v in range(len(self.task_keys)):
                    task[self.task_keys[v]] = model[iter][v]
        self.tasker.remove_tasks([task])
        self.update_treeview()
        self.indi_menu_refresh_cb(self.menu, self.tasker, self.indicator)

    def on_button_remove_all_clicked(self, widget):
        self.tasker.remove_tasks(self.tasker.tasks)
        self.update_treeview()
        self.indi_menu_refresh_cb(self.menu, self.tasker, self.indicator)

    def on_button_add_clicked(self, wigdet):
        # Regex pattern for DD-MM-YYYY
        date_pattern = '(\d{2})[-](\d{2})[-](\d{4})$'
        parsed_date = re.match(date_pattern, self.entryDate.get_text())
        if parsed_date is not None and self.entryName.get_text() is not "":
            try:
                date = datetime.datetime(
                    *(map(int, parsed_date.groups()[-1::-1]))).strftime("%d-%m-%Y")
                self.tasker.tasks.append({"name": self.entryName.get_text(
                ), "date": str(date), "priority": self.priority_selected})
            except:
                date_err_md = gtk.MessageDialog(self, type=gtk.MessageType.INFO, buttons=gtk.ButtonsType.OK,
                                                message_format="Put a data format as DD-MM-YYYY")
                date_err_md.format_secondary_text("Date format error")
                date_err_md.run()
                date_err_md.destroy()

        self.update_treeview()
        self.tasker.update_tasks_file()
        self.indi_menu_refresh_cb(self.menu, self.tasker, self.indicator)

    def on_priority_combo_changed(self, wigdet):
        self.priority_selected = self.priority_combo.get_active_text()


def edit_tasks(source, menu, indicator, tasker):
    """ Callback function to create and open Edit window"""
    edit_window = EditWindow(menu, indicator, tasker)
    edit_window.set_keep_above(True)
    edit_window.show_all()


def quit(source):
    gtk.main_quit()


def main():
    tasker = Tasker()
    indicator = ai.Indicator.new(APPINDICATOR_ID,
                                 ICON_PATH,
                                 ai.IndicatorCategory.SYSTEM_SERVICES)
    indicator.set_status(ai.IndicatorStatus.ACTIVE)
    menu = build_menu(tasker, indicator)
    indicator.set_menu(menu)
    gtk.main()


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()
