import kronos
import feedparser
import os
import json
from datetime import datetime
from time import mktime, strftime
from .models import Feed, Articles

                #cron job: every 10 minutes
@kronos.register('*/10 * * * *')
def update_articles():
    """ Feedparser all rss urls
        and update the articles in json"""

    update_period = Articles.objects.get(pk=1).feed_period
    print("Started updating the rss articles from the previous {} days".format(update_period))
    feeds = Feed.objects.all()
    items = []

    for feed in feeds:
        rss = feedparser.parse(feed.url)

        try:
            items.extend(rss["items"])
        except KeyError:
            continue

    all_articles = []
    
    for item in items:
        refs = []
        for link in item.links:
            if 'href' in link:
                refs.append(link['href'])
        item_org_link =  item.feedburner_origenclosurelink if 'feedburner_origenclosurelink' in item else ""     
        item_autor = item.author if 'author' in item else ""   
        item_link =  item.link if 'link' in item else ""   
        all_articles.append({"title":item.title,"publish_date":item.published_parsed,
                             "author":item_autor,"link":item_link,"ref_original":item_org_link, "refs":refs})
                             
    all_articles = list(reversed(sorted(all_articles, key=lambda article: article['publish_date'])))
    actual_articles = []
    for art in all_articles:
        published = datetime.fromtimestamp(mktime(art['publish_date']))
        time_delta = datetime.now() - published
            
        if time_delta.days > update_period:
            break
        else:
            art['publish_date'] = strftime('%Y-%m-%d %H:%M:%S', art['publish_date'])
            actual_articles.append(art)
    with open(os.path.dirname(os.path.realpath(__file__)) + '/static/rssapp/articles.json', 'w') as fp:
        json.dump(actual_articles, fp) 

    print("Updated the articles json file...")
