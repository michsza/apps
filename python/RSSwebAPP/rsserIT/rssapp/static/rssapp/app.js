var rssApp = new Vue({
    el: '#rss-app',
    externals: {
        '_': '_'
    },
    data: {
            items: [],
            feeds: [],
            filteredItems: [],
            newLink: "",
            route: "feeds",
            period: "",
            updatePeriod: 0, 
            invalidInput : false,
            sortKey: '',
            sortSettings: [
                { 'publish_date': true },
                { 'link': true },
                { 'author': true }
              ],
            desc: true,
            searchKey: ""
    },
    computed: {
        orderedByItems: function () {
                order = this.desc ? 'asc' : 'desc'
                return _.orderBy(this.filteredItems, this.sortKey, order);
        },
        updatePeriodValue: function() {
                return this.updatePeriod;
        },

        },
        watch:{
           searchKey: function (){
                this.filteredBy();
           }
        },
        methods: {
            api: function(endpoint, method, data) {
                    var config = {
                            method: method || 'GET',
                            body: data !== undefined ? JSON.stringify(data) : null,
                            headers: {
                                    'content-type': 'application/json'
                            }
                    };

                    return fetch(endpoint, config)
                                    .then((response) => response.json())
                                    .catch((error) => console.log(error));
            },
            reload: function() {
                    this.getItems();
                    this.getFeeds();
            },

            getFeeds: function() {
                    this.api("/rssapp/feeds/").then((feeds) => {
                            this.feeds = feeds;
                    });
            },

            getItems: function() {
                    this.api("/rssapp/items/").then((data) => {
                        this.items = data.items;
                        this.filteredItems = this.items;
                        this.updatePeriod = data.period
                    });
            },

            newFeed: function() {
                    this.api("/rssapp/feeds/", "POST", { url: this.newLink }).then(() => {
                            this.reload();
                    });
            },
            filteredBy: function(){
                this.filteredItems = this.items.filter((item) => {
                        return (item.publish_date.toString().toLowerCase().indexOf(this.searchKey.toLowerCase()) > -1 ||
                        item.link.toLowerCase().indexOf(this.searchKey.toLowerCase()) > -1 ||
                        item.author.toLowerCase().indexOf(this.searchKey.toLowerCase()) > -1);
                    })
            },
            orderBy: function(sorKey) {
                this.sortKey = sorKey
                this.sortSettings[sorKey] = !this.sortSettings[sorKey]
                this.desc = this.sortSettings[sorKey]
            },
            periodValidation: function() {
                if ((this.period % 1 === 0)
                        && (this.period > 0))
                        {
                         this.updatePeriod = this.period
                         this.invalidInput = false
                         return true
                        }
                else {
                        this.invalidInput = true
                        return false
                }
        },
            newPeriod: function() {
                properInput = this.periodValidation();
                if (properInput) {
                        this.api("/rssapp/period/", "PUT", { feed_period: this.period }).then(() => {
                        this.updatePeriod = this.period;
                        this.reload();
                });
                }
        },


            deleteFeed: function(id) {
                    this.api("/rssapp/feeds/" + id + "/", "DELETE").then(() => {
                            this.reload();
                    });
            },

            setRoute: function(route) {
                this.route = route;
            },

            setup: function() {
                var hash = window.location.hash;
        
                if(hash) {
                        this.route = hash.slice(1);
                }
        
                this.reload();
            }
    }
});