from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import feedparser
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .serializers import FeedSerializer, ArticlesSerializer
from .models import Feed, Articles
import json
import os
from datetime import datetime
from time import mktime, strftime


def index(request):
    return render(request, 'rssapp/reader.html')


@csrf_exempt
def rest_feeds(request):
    if request.method == "GET":
        feeds = Feed.objects.all()
        serializer = FeedSerializer(feeds, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == "POST":
        data = JSONParser().parse(request)
        serializer = FeedSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)

        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def rest_feeds_detail(request, pk):
    try:
        feed = Feed.objects.get(pk=pk)
    except Feed.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == "GET":
        serializer = FeedSerializer(feed)
        return JsonResponse(serializer.data)

    elif request.method == "PUT":
        data = JSONParser().parse(request)
        serializer = FeedSerializer(feed, data=data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)

        return JsonResponse(serializer.errors, status=400)

    elif request.method == "DELETE":
        feed.delete()
        return HttpResponse()


@csrf_exempt
def rest_items(request):
    with open(os.getcwd() + '/rssapp/static/rssapp/articles.json', 'r') as fp:
        actual_articles = json.load(fp)

    if not Articles.objects.filter(pk=1).exists():
        serializer = ArticlesSerializer(data={'feed_period:':'21'})
        if serializer.is_valid():
            serializer.save()

    update_period = Articles.objects.get(pk=1).feed_period
    jsonRsp = {'items': actual_articles, 'period': update_period}
    return JsonResponse(jsonRsp, safe=False)

@csrf_exempt
def rest_period(request):
    articles = Articles.objects.get(pk=1)
    
    if request.method == "GET":
        serializer = ArticlesSerializer(feeds, many=True)
        return JsonResponse(serializer.data, safe=False)
    
    elif request.method == "PUT":
        data = JSONParser().parse(request)
        serializer = ArticlesSerializer(articles, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == "POST":
        return HttpResponse(status=400)

    return JsonResponse(serializer.errors, status=400)