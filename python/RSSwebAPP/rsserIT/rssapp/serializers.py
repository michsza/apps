from rest_framework import serializers
from .models import Feed, Articles

class FeedSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Feed
        fields = ('id', 'url')

class ArticlesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Articles
        fields = ('feed_period',)