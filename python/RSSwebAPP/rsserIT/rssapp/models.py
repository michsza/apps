from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

class Feed(models.Model):
    url = models.URLField(max_length=255, unique=True)
    def __repr__(self):
        return "<Feed '{}'>".format(self.url)


class SingletonModel(models.Model):

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj

class Articles(models.Model):
    # fetch period (days) to update feeds, starting from todays date
    feed_period = models.IntegerField(default=21, validators=[MinValueValidator(0),
                                       MaxValueValidator(5*365)], unique=True)
    class Meta:
        managed = True
